<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ProductPrice>
 */
class ProductPriceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $base = rand(10000,15000);
        return [
            'price' => $base,
            'published_price' => $base + 1000,
            'buy_price' => $base - 8000,
            'price_type' => 'retail',
            'product_id' => 1,
            'target_type' => 'ENDUSER'
        ];
    }
}
