<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ProductPriceWholesaler>
 */
class ProductPriceWholesalerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'min' => 1,
            'max' => 1,
            'price' => 1,
            'varian_id' => 1,
            'product_price_id' => 1
        ];
    }
}
