<?php

namespace Database\Factories;

use App\Models\PostalCode;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Address>
 */
class AddressFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'region_id' => 21,
            'city_id' => 313,
            'district_id' => 4743,
            'subdistrict_id' => 58257,
            'postal_code_id' => 73971,
            'street' => $this->faker->words(4, true),
            'user_id' => 1
        ];
    }
}
