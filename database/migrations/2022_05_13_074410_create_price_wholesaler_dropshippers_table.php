<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_wholesaler_dropshippers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('min')->default(0);
            $table->unsignedBigInteger('max')->default(0);
            $table->bigInteger('price')->default(0);
            $table->unsignedBigInteger('price_dropshipper_id');
            $table->integer('varian_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_wholesaler_dropshippers');
    }
};
