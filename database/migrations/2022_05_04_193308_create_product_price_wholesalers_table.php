<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_price_wholesalers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('min')->default(0);
            $table->unsignedBigInteger('max')->default(0);
            $table->bigInteger('price')->default(0);
            $table->unsignedBigInteger('product_price_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_price_wholesalers');
    }
};
