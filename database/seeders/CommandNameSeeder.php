<?php

namespace Database\Seeders;

use App\Models\CommandName;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CommandNameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'increase'],
            ['name' => 'decrease']
        ];

        CommandName::insert($data);
    }
}
