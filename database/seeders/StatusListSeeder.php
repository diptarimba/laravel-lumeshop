<?php

namespace Database\Seeders;

use App\Models\StatusList;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class StatusListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['command_name_id' => 1, 'status' => 'increase by admin'],
            ['command_name_id' => 2, 'status' => 'decrease by admin']
        ];

        StatusList::insert($data);
    }
}
