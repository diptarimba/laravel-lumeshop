<?php

namespace Database\Seeders;

use App\Models\About;
use App\Models\ResellerLinkType;
use App\Models\Subdistrict;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(FaqSeeder::class);
        $this->call(AboutSeeder::class);
        $this->call(ProductCategorySeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(UserRoleSeeder::class);
        $this->call(ResellerLinkTypeSeeder::class);
        $this->call(CommandNameSeeder::class);
        $this->call(StatusListSeeder::class);
        $this->call(ProductSeeder::class);

        //Address
        $this->call(RegionSeeder::class);
        $this->call(CitySeeder::class);
        $this->call(DistrictSeeder::class);
        $this->call(SubdistrictSeeder::class);
        $this->call(PostalCodeSeeder::class);

        $this->call(UserSeeder::class);
        $this->call(CoinSeeder::class);
        $this->call(CustomerSeeder::class);
        $this->call(ResellerSeeder::class);
        $this->call(AddressSeeder::class);
        // \App\Models\User::factory(10)->create();
    }
}
