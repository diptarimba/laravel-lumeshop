<?php

namespace Database\Seeders;

use App\Models\PivotProductSubcategory;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductPrice;
use App\Models\ProductPriceWholesaler;
use App\Models\ProductSubcategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::factory()->count(100)->create()->map(function($product){
            ProductImage::factory()
                    ->count(rand(1,10))
                    ->state(['product_id' => $product->id])
                    ->create();
            ProductPrice::factory()
                ->count(2)
                ->state(new Sequence(
                    ['price_type' => 'wholesale', 'target_type' => 'ENDUSER', 'product_id' => $product->id],
                    ['price_type' => 'wholesale', 'target_type' => 'RESELLER', 'product_id' => $product->id]
                ))
                ->create()->where('price_type', 'wholesale')->map(function($productPrice){
                    ProductPriceWholesaler::factory()
                        ->count(5)
                        ->state(new Sequence(
                            ['min' => 1, 'max' => 10, 'price' => $productPrice->price - 100, 'varian_id' => 1, 'product_price_id' => $productPrice->id],
                            ['min' => 2, 'max' => 20, 'price' => $productPrice->price - 200, 'varian_id' => 2, 'product_price_id' => $productPrice->id],
                            ['min' => 3, 'max' => 30, 'price' => $productPrice->price - 300, 'varian_id' => 3, 'product_price_id' => $productPrice->id],
                            ['min' => 4, 'max' => 40, 'price' => $productPrice->price - 400, 'varian_id' => 4, 'product_price_id' => $productPrice->id],
                            ['min' => 5, 'max' => 50, 'price' => $productPrice->price - 500, 'varian_id' => 5, 'product_price_id' => $productPrice->id]
                        ))
                        ->create();
                });
            $productSubcategory = ProductSubcategory::all()->random(1)->first();
            PivotProductSubcategory::create([
                'product_id' => $product->id,
                'product_subcategory_id' => $productSubcategory->id,
                'status' => 1
            ]);
        });
    }
}
