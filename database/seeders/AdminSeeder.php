<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        for($x =0; $x < 10; $x++){
            $data[] = [
                'username' => 'admin'.$x,
                'password' => Hash::make('12345678'),
                'name' => Str::random(5) . ' ' . Str::random(5),
                'email' => 'dev' . $x . '@gmail.com',
                'phone' => '+628' . random_int(100000,100000),
                'role' => $x % 2 ? 'ADMIN' : 'SUPERADMIN'
            ];
        }

        Admin::insert($data);
    }
}
