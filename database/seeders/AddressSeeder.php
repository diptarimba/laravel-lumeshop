<?php

namespace Database\Seeders;

use App\Models\Address;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::get()->pluck('id');
        $user->map(function($id){
            return Address::factory()
                ->count(rand(1,5))
                ->state([
                    'user_id' => $id
                ])
                ->create();
        });
    }
}
