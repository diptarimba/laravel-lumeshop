<?php

namespace Database\Seeders;

use App\Models\ResellerLinkType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ResellerLinkTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'Shopee'],
            ['name' => 'Tokopedia'],
            ['name' => 'Bukalapak'],
            ['name' => 'Lazada'],
        ];

        ResellerLinkType::insert($data);
    }
}
