<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Flynsarmy\CsvSeeder\CsvSeeder;

class PostalCodeSeeder extends CsvSeeder
{
    public function __construct()
	{
		$this->table = 'postal_codes';
        $this->csv_delimiter = ',';
        $this->offset_rows = 1;
		$this->filename = base_path().'/database/seeds/Address/postal_code.csv';
        $this->mapping = [
            5 => 'code',
            4 => 'region_id',
            3 => 'city_id',
            2 => 'district_id',
            1 => 'subdistrict_id',
        ];
        $this->should_trim = true;
	}

	public function run()
	{
		// Recommended when importing larger CSVs
		// DB::disableQueryLog();

		// Uncomment the below to wipe the table clean before populating
		// DB::table($this->table)->truncate();

		parent::run();
	}
}
