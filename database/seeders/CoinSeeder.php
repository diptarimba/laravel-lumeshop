<?php

namespace Database\Seeders;

use App\Models\Coin;
use App\Models\CoinHistory;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class CoinSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::pluck('id')->map(function($id){
            return Coin::factory()->count(1)->state(['user_id' => $id])->create()->map(function($coin){
                return CoinHistory::factory()
                    ->count(4)
                    ->state(new Sequence(
                        ['command_name_id' => 1, 'coin_id' => $coin->id],
                        ['command_name_id' => 2, 'coin_id' => $coin->id]
                    ))
                    ->create();
            });
        });
    }
}
