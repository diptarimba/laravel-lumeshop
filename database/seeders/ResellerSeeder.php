<?php

namespace Database\Seeders;

use App\Models\Reseller;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ResellerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $resellerUser = User::whereRolesId(2)->pluck('id');
        $resellerUser->map(function($id){
            return Reseller::factory()->count(1)->state(['user_id' => $id])->create();
        });
    }
}
