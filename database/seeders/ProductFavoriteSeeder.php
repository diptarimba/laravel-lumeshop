<?php

namespace Database\Seeders;

use App\Models\Favorite;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductFavoriteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        array_map(function($product){
            array_map(function($user) use($product){
                $bool = rand(0,1);
                if($bool){
                    Favorite::create([
                        'user_id' => $user,
                        'product_id' => $product
                    ]);
                }
            }, User::pluck('id')->toArray());
        }, Product::pluck('id')->toArray());
    }
}
