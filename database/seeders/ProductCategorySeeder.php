<?php

namespace Database\Seeders;

use App\Models\ProductCategory;
use App\Models\ProductSubcategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductCategory::factory()
        ->count(20)
        ->create()
        ->map(function($productCategory){
            return ProductSubcategory::factory()
                ->count(rand(5,20))
                ->state([
                    'product_category_id' => $productCategory->id,
                ])->create();
        });
    }
}
