<?php

use App\Http\Controllers\Admin\AboutController;
use App\Http\Controllers\Admin\AddressController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\CartController;
use App\Http\Controllers\Admin\CityController;
use App\Http\Controllers\Admin\CoinController;
use App\Http\Controllers\Admin\CustomerController;
use App\Http\Controllers\Admin\DistrictController;
use App\Http\Controllers\Admin\FaqController;
use App\Http\Controllers\Admin\FavoriteController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\PaymentMethodController;
use App\Http\Controllers\Admin\ProductCategoryController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\ProductImageController;
use App\Http\Controllers\Admin\ProductPriceController;
use App\Http\Controllers\Admin\ProductSubcategoryController;
use App\Http\Controllers\Admin\RegionController;
use App\Http\Controllers\Admin\ResellerController;
use App\Http\Controllers\Admin\ShippingController;
use App\Http\Controllers\Admin\SliderPromoController;
use App\Http\Controllers\Admin\VoucherController;
use App\Http\Controllers\AdminLoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/login', [AdminLoginController::class, 'index'])->name('admin.login.index');
Route::post('/post', [AdminLoginController::class, 'login'])->name('admin.login');
Route::get('/logout', [AdminLoginController::class, 'logout'])->name('admin.logout');

Route::get('/', function(){
    return redirect(route('admin.login.index'));
});

Route::group(['middleware'=>'auth:admin'], function() {
    Route::get('/', [HomeController::class, 'index'])->name('dashboard');
    Route::resource('/admin', AdminController::class);
    Route::resource('/about', AboutController::class);
    Route::resource('/address', AddressController::class);
    Route::resource('/cart', CartController::class);
    Route::resource('/coin', CoinController::class);
    Route::resource('/customer', CustomerController::class);
    Route::resource('/faq', FaqController::class);
    Route::resource('/favorite', FavoriteController::class);
    Route::resource('/order', OrderController::class);
    Route::resource('/product', ProductController::class );
    Route::resource('/payment_method', PaymentMethodController::class);
    Route::resource('/product_category', ProductCategoryController::class);
    Route::resource('/product_subcategory', ProductSubcategoryController::class);
    Route::resource('/product_image', ProductImageController::class);
    Route::resource('/product_price', ProductPriceController::class);
    Route::resource('/reseller', ResellerController::class);
    Route::resource('/shipping', ShippingController::class);
    Route::resource('/slider_promo', SliderPromoController::class);
    Route::resource('/voucher', VoucherController::class);
    Route::resource('/city', CityController::class);
    Route::resource('/region', RegionController::class);
    Route::resource('/district', DistrictController::class);

    Route::delete('/product_price/wholesaler/{wholesaler}', [ProductPriceController::class, 'destroyWholesaler'])->name('wholesaler.delete');
    Route::get('/coin/{coin}/history', [CoinController::class, 'coinhistory']);
});


//Admin Home page after login


