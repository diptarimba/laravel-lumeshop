@extends('layout.page')

@section('title', 'Product')

@section('header-content')

@endsection

@section('page-content')
<div class="col-md-12">
    <div class="row justify-content-center mb-2">
        <div class="col-md-3 mx-1 my-1">
            <div class="card">
                <div class="card-header">
                    Total Produk
                </div>
                <div class="card-body">
                    {{number_format($totalProduct, 0, ',', '.')}}
                </div>
            </div>
        </div>
        <div class="col-md-3 mx-1 my-1">
            <div class="card">
                <div class="card-header">
                    Total Customer
                </div>
                <div class="card-body">
                    {{number_format($totalCustomer, 0, ',', '.')}}
                </div>
            </div>
        </div>
        <div class="col-md-3 mx-1 my-1">
            <div class="card">
                <div class="card-header">
                    Total Reseller
                </div>
                <div class="card-body">
                    {{number_format($totalReseller, 0, ',', '.')}}
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-3 mx-1 my-1">
            <div class="card">
                <div class="card-header">
                    Penjualan Hari Ini
                </div>
                <div class="card-body">
                    1,000
                </div>
            </div>
        </div>
        <div class="col-md-3 mx-1 my-1">
            <div class="card">
                <div class="card-header">
                    Penjualan Kemarin
                </div>
                <div class="card-body">
                    500
                </div>
            </div>
        </div>
        <div class="col-md-3 mx-1 my-1">
            <div class="card">
                <div class="card-header">
                    Penjualan All Time
                </div>
                <div class="card-body">
                    200
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer-content')

@endsection
