@extends('layout.page')

@section('title', 'Slider Promo')

@section('header-content')

@endsection

@section('page-content')
<x-card.layout >

    <x-slot name="header">
        <x-card.h-buat url="{{route('slider_promo.create')}}" title="Slider Promo"/>
    </x-slot>

    <x-slot name="body">
        <table class="table table-striped">
            <thead>
                <th>No</th>
                <th>Image</th>
                <th>Name</th>
                <th>Expired Date</th>
                <th>Action</th>
            </thead>
            <tbody>
                @foreach ($sliderPromos as $each)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td><img src="{{Storage::url($each->image)}}" alt="" class="img-fluid"></td>
                        <td>{{$each->name}}</td>
                        <td>{{$each->exp_date}}</td>
                        <td>
                            <x-action.edit action="{{route('slider_promo.edit', $each->id)}}" />
                            <x-action.delete :ident="$each->id" action="{{route('slider_promo.destroy', $each->id)}}" />
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </x-slot>
</x-card.layout>
@endsection

@section('footer-content')

@endsection
