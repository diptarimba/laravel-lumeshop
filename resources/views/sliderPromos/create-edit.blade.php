@extends('layout.page')

@section('title', 'Slider Promo')

@section('header-content')

@endsection

@section('page-content')
<x-card.layout >

    <x-slot name="header">
        {{ request()->routeIs('slider_promo.create') ? 'Create Slider Promo' : 'Update Slider Promo' }}
    </x-slot>
    <x-slot name="body">
        <form id="form-slider_promo"
            action="{{ request()->routeIs('slider_promo.create') ? route('slider_promo.store'): route('slider_promo.update', $sliderPromo->id) }}"
            class="theme-form mega-form mb-3" method="post"
            enctype="multipart/form-data">
            @csrf
            <x-forms.put-method  />
            <x-forms.text name="name" label="Nama Slider" :value="@$sliderPromo->name"/>
            <x-forms.date name="exp_date" label="Tanggal Slider Berakhir" value=""/>
            @if (request()->routeIs('slider_promo.edit'))
                <img :src="@$sliderPromo->image" alt="" style="max-width: 200px">
            @endif
            <x-forms.file name="image" label="Gambar Slider" />
        </form>
        <button form="form-slider_promo" class="btn btn-primary btn-pill">Submit</button>
        <x-action.cancel />
    </x-slot>
</x-card.layout>
@endsection

@section('footer-content')

@endsection
