@extends('layout.page')

@section('title', 'Product Subcategory')

@section('header-content')

@endsection

@section('page-content')
<x-card.layout >
    <x-slot name="header">
        <x-card.h-buat url="{{route('product_subcategory.create')}}" title="Product Subcategory"/>
    </x-slot>

    <x-slot name="body">
        <table class="table table-striped">
            <thead>
                <th>No</th>
                <th>Name</th>
                <th>Category</th>
                <th>Action</th>
            </thead>
            <tbody>
                @foreach ($productSubcategories as $each)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$each->name}}</td>
                        <td>{{$each->product_category->name}}</td>
                        <td>
                            <x-action.edit action="{{route('product_subcategory.edit', $each->id)}}" />
                            <x-action.delete :ident="$each->id" action="{{route('product_subcategory.destroy', $each->id)}}" />
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </x-slot>
</x-card.layout>
@endsection

@section('footer-content')

@endsection
