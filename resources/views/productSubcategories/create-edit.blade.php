@extends('layout.page')

@section('title', 'Product Subcategory')

@section('header-content')

@endsection

@section('page-content')
<x-card.layout >
    <x-slot name="header">
        {{ request()->routeIs('product_subcategory.create') ? 'Create Product Subcategory' : 'Update Product Subcategory' }}
    </x-slot>
    <x-slot name="body">
        <form id="form"
            action="{{ request()->routeIs('product_subcategory.create') ? route('product_subcategory.store'): route('product_subcategory.update', @$productSubcategory->id) }}"
            class="theme-form mega-form" method="post"
            enctype="multipart/form-data">
            @csrf
            <x-forms.put-method  />
            <x-forms.input label="Name" name="name" :value="@$productSubcategory->name"/>
            <x-forms.select name="product_category_id" label="Product Category" :items="@$productCategory" :value="@$productSubcategory->product_category_id"/>
        </form>
        <button form="form" class="btn btn-primary btn-pill">Submit</button>
        <x-action.cancel />
    </x-slot>
</x-card.layout>
@endsection

@section('footer-content')

@endsection
