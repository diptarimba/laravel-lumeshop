@extends('layout.page')

@section('title', 'FAQ')

@section('header-content')

@endsection

@section('page-content')
<x-card.layout >
    <x-slot name="header">
        <x-card.h-buat url="{{route('faq.create')}}" title="Frequently Asked Question (FAQ)"/>
    </x-slot>

    <x-slot name="body">
        <table class="table table-striped">
            <thead>
                <th>Question</th>
                <th>Answer</th>
                <th>Action</th>
            </thead>
            <tbody>
                @foreach ($faqs as $item)
                    <tr>
                        <td>{{$item->question}}</td>
                        <td>{{$item->answer}}</td>
                        <td>
                            <x-action.edit :ident="$item->id" :action="route('faq.edit', $item->id)" />
                            <x-action.delete :ident="$item->id" :action="route('faq.destroy', $item->id)" />
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </x-slot>
</x-card.layout>
@endsection

@section('footer-content')

@endsection
