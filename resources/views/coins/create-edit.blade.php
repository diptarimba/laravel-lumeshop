@extends('layout.page')

@section('title', 'Coin')

@section('header-content')
<link rel="stylesheet" href="//cdn.datatables.net/1.12.0/css/jquery.dataTables.min.css">
@endsection

@section('page-content')
<div class="row col-md-12 mx-auto">
    <div class="col-md-6">
        <x-card.layout>
            <x-slot name="header">
                Data Coin
            </x-slot>
            <x-slot name="body">
                <x-forms.text label="Nama" name="name" :value="$coin->user->name" />
                <x-forms.input label="Saldo Coin" name="coin" :value="$coin->value" type="number"/>
            </x-slot>
        </x-card.layout>
    </div>
    <div class="col-md-6">
        <x-card.layout>
            <x-slot name="header">
                {{ request()->routeIs('coin.create') ? 'Tambah/Kurangi Coin' : 'Update Coin' }}
            </x-slot>
            <x-slot name="body">
                <form id="form"
                    action="{{ request()->routeIs('coin.create') ? route('coin.store'): route('coin.update', @$coin->id) }}"
                    class="theme-form mega-form" method="post" enctype="multipart/form-data">
                    @csrf
                    <x-forms.put-method />
                    <x-forms.select name="operation" label="Operasi" :items="@$choice"
                        value="increase" />
                    <x-forms.input name="amount" required type="number" label="Nominal" :value="0"/>
                </form>
                <button form="form" class="btn btn-primary btn-pill">Update</button>
                <x-action.cancel />
            </x-slot>
        </x-card.layout>
    </div>
</div>
<x-card.layout>
    <x-slot name="header">
        History Coin
    </x-slot>
    <x-slot name="body">
        <table class="table table-striped" id="table-history">
            <thead>
                <th>No</th>
                {{-- <th>Id</th> --}}
                {{-- <th>Operation</th> --}}
                <th>Status</th>
                <th>Amount</th>
                <th>Created At</th>
            </thead>
        </table>
    </x-slot>
</x-card.layout>
@endsection

@section('footer-content')
<script src="https://code.jquery.com/jquery.js"></script>
<script src="//cdn.datatables.net/1.12.0/js/jquery.dataTables.min.js"></script>
<script>
        $(document).ready(() => {
            $('#table-history').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('coin.edit', Route::current()->parameters['coin']->id) !!}', // memanggil route yang menampilkan data json
                columns: [
                    {
                        "data": '',
                        render: function (data, type, row, meta) {
                                    return meta.row + meta.settings._iDisplayStart + 1;
                                }
                    },
                    {
                        data: 'command_name.status_list.status',
                        name: 'status',
                    },
                    {
                        data: 'amount',
                        name: 'amount'
                    },
                    {
                        data: 'created_at',
                        name: 'created_at'
                    },
                ]
            });
        });
    </script>
@endsection
