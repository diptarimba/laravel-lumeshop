@extends('layout.page')

@section('title', 'About')

@section('header-content')
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
@endsection

@section('page-content')
<div class="mt-4">
    <div class="col-12 col-xl-10 order-1 order-xl-0">
        <div class="card shadow-none border border-300 my-4" data-component-card>
            <div class="card-header p-4 border-bottom border-300 bg-soft">
              <div class="row g-3 justify-content-between align-items-center">
                <div class="col-12 col-md">
                  <h3 class="text-900 mb-0" data-anchor> Title </h3>
                </div>
              </div>
            </div>
            <div class="card-body">
                <form action="{{ route('about.update', $about->id)}}" method="post">
                    @csrf
                    @method('PUT')
                    <textarea name="text" id="summernote" cols="30" rows="10">{{$about->text}}</textarea>
                    <button type="submit" class="btn btn-primary">Perbarui</button>
                </form>
            </div>
          </div>
    </div>
</div>
@endsection

@section('footer-content')
<script>
    $(document).ready(()=> {
        $('#summernote').summernote({
          placeholder: 'Hello Bootstrap 5',
          tabsize: 2,
          height: 100
        });
    })
  </script>
@endsection
