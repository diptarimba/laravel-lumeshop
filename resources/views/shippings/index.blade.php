@extends('layout.page')

@section('title', 'Shipping')

@section('header-content')

@endsection

@section('page-content')
<x-card.layout >
    <x-slot name="header">
        <x-card.h-buat url="{{route('shipping.create')}}" title="Shipping"/>
    </x-slot>

    <x-slot name="body">
        <table class="table table-striped">
            <thead>

            </thead>
            <tbody>

            </tbody>
        </table>
    </x-slot>
</x-card.layout>
@endsection

@section('footer-content')

@endsection
