@extends('layout.page')

@section('title', 'Customer')

@section('header-content')

@endsection

@section('page-content')
<x-card.layout>
    <x-slot name="header">
        {{ request()->routeIs('customer.create') ? 'Create Customer' : 'Update Customer' }}
    </x-slot>
    <x-slot name="body">
        <form id="form"
            action="{{ request()->routeIs('customer.create') ? route('customer.store'): route('customer.update', @$customer->id) }}"
            class="theme-form mega-form" method="post" enctype="multipart/form-data">
            @csrf
            <x-forms.put-method />
            <input type="hidden" name="roles_id" value="customer">
            <x-forms.input required="" label="Nama Lengkap" name="name" :value="@$customer->user->name" />
            <x-forms.input required="" label="Email" name="email" :value="@$customer->user->email" />
            <x-forms.input required="" label="Username" name="username" :value="@$customer->user->username" />
            <x-forms.date label="Tanggal Lahir" name="birthdate" :value="@$customer->user->birthdate" />
            <x-forms.input required="" label="Phone Number" type="number" name="phone_number" :value="@$customer->user->phone_number" />
            <x-forms.text password="1" label="Password" name="password" :value="@$customer->user->passwords" />
        </form>
        <button form="form" class="btn btn-primary btn-pill">Submit</button>
        <x-action.cancel />
    </x-slot>
</x-card.layout>
@endsection

@section('footer-content')

@endsection
