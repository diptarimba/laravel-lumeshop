@extends('layout.page')

@section('title', 'Voucher')

@section('header-content')
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
@endsection

@section('page-content')
<x-card.layout >
    <x-slot name="header">
        {{ request()->routeIs('voucher.create') ? 'Create Voucher' : 'Update Voucher' }}
    </x-slot>
    <x-slot name="body">
        <form id="form"
            action="{{ request()->routeIs('voucher.create') ? route('voucher.store'): route('voucher.update', @$voucher->id) }}"
            class="theme-form mega-form" method="post"
            enctype="multipart/form-data">
            @csrf
            <x-forms.put-method  />
            <x-forms.input label="Kode" name="code" :value="@$voucher->code"/>
            <button type="button" class="btn btn-warning generate">Generate Code</button>
            <x-forms.select name="type" label="Voucher Type" :items="@$voucherType" :value="@$voucher->type"/>
        </form>
        <button form="form" class="btn btn-primary btn-pill">Submit</button>
        <x-action.cancel />
    </x-slot>
</x-card.layout>
@endsection

@section('footer-content')
<script>
    $(document).ready(() => {
        function makeid(length) {
            var result           = '';
            var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
            var charactersLength = characters.length;
            for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() *
        charactersLength));
        }
        return result;
        }

        $('.generate').on('click', function(){
            $('input[name="code"]').val(makeid(5));
        })
    })
</script>
@endsection
