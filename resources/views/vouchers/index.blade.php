@extends('layout.page')

@section('title', 'Voucher')

@section('header-content')

@endsection

@section('page-content')
<x-card.layout >
    <x-slot name="header">
        <x-card.h-buat url="{{route('voucher.create')}}" title="Voucher"/>
    </x-slot>

    <x-slot name="body">
        <table class="table table-striped">
            <thead>
                <th>No</th>
                <th>Code</th>
                <th>Type</th>
                <th>Action</th>
            </thead>
            <tbody>
                @foreach ($vouchers as $each)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$each->code}}</td>
                        <td>{{$each->type}}</td>
                        <td>
                            <x-action.edit action="{{route('voucher.edit', $each->id)}}" />
                            <x-action.delete :ident="$each->id" action="{{route('voucher.destroy', $each->id)}}" />
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </x-slot>
</x-card.layout>
@endsection

@section('footer-content')

@endsection
