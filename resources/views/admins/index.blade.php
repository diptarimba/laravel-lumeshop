@extends('layout.page')

@section('title', 'Customer')

@section('header-content')

@endsection

@section('page-content')
<x-card.layout >
    <x-slot name="header">
        <x-card.h-buat url="{{route('admin.create')}}" title="Admin"/>
    </x-slot>

    <x-slot name="body">
        <table class="table table-striped">
            <thead>
                <th>No</th>
                <th>Nama</th>
                <th>Username</th>
                <th>Role</th>
                <th>Action</th>
            </thead>
            <tbody>
                @foreach ($admins as $each)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$each->name}}</td>
                        <td>{{$each->username}}</td>
                        <td>{{$each->role}}</td>
                        <td class="d-flex">
                            <x-action.edit action="{{route('admin.edit', $each->id)}}" />
                            <x-action.delete :ident="$each->id" action="{{route('admin.destroy', $each->id)}}" />
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </x-slot>
</x-card.layout>
@endsection

@section('footer-content')

@endsection
