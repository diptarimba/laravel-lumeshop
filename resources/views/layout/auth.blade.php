@extends('layout.master')
@section('header')
<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/favicons/apple-touch-icon.png') }}">
<link rel="icon" type="image/png" href="{{ asset('img/icons/logo.png') }}">
<link rel="manifest" href="{{ asset('img/favicons/manifest.json') }}">
<meta name="msapplication-TileImage" content="{{ asset('img/favicons/mstile-150x150.png') }}">
<meta name="theme-color" content="#ffffff">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&amp;display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;1,100;1,200;1,300;1,400;1,500;1,600;1,700&amp;display=swap" rel="stylesheet">
<link href="{{ asset('css/phoenix.min.css') }}" rel="stylesheet" id="style-default">
<link href="{{ asset('css/user.min.css') }}" rel="stylesheet" id="user-style-default">
<style>
  body {
    opacity: 0;
  }
</style>
@yield('header-content')
@endsection

@section('body')
<main class="main" id="top" style="background-image: url('{{asset('img/background/bg-web-admin-lume.jpg')}}');">
    <div class="container-fluid px-0">
        @yield('page-content')
    </div>
  </main>
@endsection

@section('footer')
<script src="{{asset('js/phoenix.js')}}"></script>
@yield('footer-content')
@endsection
