@extends('layout.master')
@section('header')

<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/favicons/apple-touch-icon.png') }}">
<link rel="icon" type="image/png" href="{{ asset('img/icons/logo.png') }}">
<link rel="manifest" href="{{ asset('img/favicons/manifest.json') }}">
<meta name="msapplication-TileImage" content="{{ asset('img/favicons/mstile-150x150.png') }}">
<meta name="theme-color" content="#ffffff">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="{{asset('css/googlefont/css2.css')}}" rel="stylesheet">
<link href="{{asset('css/googlefont/css2-2.css')}}" rel="stylesheet">
<link href="{{ asset('css/phoenix.min.css') }}" rel="stylesheet" id="style-default">
<link href="{{ asset('css/user.min.css') }}" rel="stylesheet" id="user-style-default">
<!-- include summernote css/js -->
<link href="{{ asset('css/summernote/summernote.min.css')}}" rel="stylesheet">
<script src="{{asset('js/jquery-3.5.1.min.js') }}"></script>
<script src="{{ asset('js/summernote/summernote.min.js') }}"></script>
<link rel="stylesheet" href="{{asset('css/fontawesome/all.min.css')}}" rel="stylesheet"/>
<script src="{{ asset('js/fontawesome/all.min.js')}}" ></script>
<script src="{{ asset('js/Sweetalert2/sweetalert2@11.js') }}"></script>
<style>
    body {
        opacity: 0;
    }
</style>

@yield('header-content')
@stack('header-add')
@endsection

@section('body')
<main class="main" id="top">
    <div class="container-fluid px-0">


        @component('components.sidebar')
        @endcomponent

        <div class="content">
            @component('components.flash')
            @endcomponent
            @yield('page-content')
            <footer class="footer">
                <div class="row g-0 justify-content-between align-items-center h-100 mb-3">
                    <div class="col-12 col-sm-auto text-center">
                        <p class="mb-0 text-900"><span class="d-none d-sm-inline-block"></span><span
                                class="mx-1">|</span><br class="d-sm-none">2022 &copy; <a
                                href="https://themewagon.com">Lume Cosmetic</a></p>
                    </div>
                    <div class="col-12 col-sm-auto text-center">
                        <p class="mb-0 text-600">v1.1.0</p>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</main>
@endsection

@section('footer')
<script src="{{asset('js/phoenix.js')}}"></script>
@yield('footer-content')
@stack('footer-add')
@endsection
