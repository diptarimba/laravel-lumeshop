@extends('layout.page')

@section('title', 'Favorite')

@section('header-content')

@endsection

@section('page-content')
<x-card.layout >
    <x-slot name="header">
        <div class="d-flex justify-content-between">
            <h3 class="text-900 mb-0" data-anchor> Favorite Product </h3>
        </div>
    </x-slot>

    <x-slot name="body">
        <table class="table table-striped">
            <thead>
                <th>No</th>
                <th>Nama Barang</th>
                <th>Jumlah Favorit</th>
            </thead>
            <tbody>
                @foreach ($favorites as $each)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$each->product->name}}</td>
                        <td>{{$each->count}}</td>
                        <td></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </x-slot>
</x-card.layout>
@endsection

@section('footer-content')

@endsection
