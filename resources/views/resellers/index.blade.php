@extends('layout.page')

@section('title', 'Reseller')

@section('header-content')

@endsection

@section('page-content')
<x-card.layout >
    <x-slot name="header">
        <x-card.h-buat url="{{route('reseller.create')}}" title="Reseller"/>
    </x-slot>

    <x-slot name="body">
        <table class="table table-striped">
            <thead>
                <th>No</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Action</th>
            </thead>
            <tbody>
                @foreach ($resellers as $each)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$each->user->name}}</td>
                        <td>{{$each->user->email}}</td>
                        <td>{{$each->user->phone_number}}</td>
                        <td class="text-center">
                            <div class="d-flex">
                                <x-action.edit action="{{route('reseller.edit', $each->id)}}" />
                                <x-action.delete :ident="$each->id" action="{{route('reseller.destroy', $each->id)}}" />
                            </div>
                            <a href="{{route('coin.edit', $each->user->coin->id)}}" class="btn btn-sm mx-1 my-1 btn-primary">Update Coin</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </x-slot>
</x-card.layout>
@endsection

@section('footer-content')

@endsection
