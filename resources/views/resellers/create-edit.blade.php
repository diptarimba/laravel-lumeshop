@extends('layout.page')

@section('title', 'Reseller')

@section('header-content')

@endsection

@section('page-content')
<x-card.layout>
    <x-slot name="header">
        {{ request()->routeIs('reseller.create') ? 'Create Reseller' : 'Update Reseller' }}
    </x-slot>
    <x-slot name="body">
        <form id="form"
            action="{{ request()->routeIs('reseller.create') ? route('reseller.store'): route('reseller.update', @$reseller->id) }}"
            class="theme-form mega-form" method="post" enctype="multipart/form-data">
            @csrf
            <x-forms.put-method />
            <input type="hidden" name="roles_id" value="reseller">
            <x-forms.input required="" label="Nama Lengkap" name="name" :value="@$reseller->user->name" />
            <x-forms.input required="" label="Email" name="email" :value="@$reseller->user->email" />
            <x-forms.input required="" label="Username" name="username" :value="@$reseller->user->username" />
            <x-forms.date label="Tanggal Lahir" name="birthdate" :value="@$reseller->user->birthdate" />
            <x-forms.input required="" label="Phone Number" type="number" name="phone_number" :value="@$reseller->user->phone_number" />
            <x-forms.text password="1" label="Password" name="password" :value="@$reseller->user->passwords" />

        </form>
        <button form="form" class="btn btn-primary btn-pill">Submit</button>
        <x-action.cancel />
    </x-slot>
</x-card.layout>
@endsection

@section('footer-content')

@endsection
