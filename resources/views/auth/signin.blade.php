@extends('layout.auth')
@section('title', 'Sign In')

@section('page-content')
<div class="container">
    <div class="row flex-center min-vh-100 py-5">
        <div class="col-sm-10 col-md-8 col-lg-5 col-xl-5 col-xxl-3"><a
                class="d-flex flex-center text-decoration-none mb-4" href="../../../index.html">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex align-items-center"><img src="{{asset('img/icons/logo.png')}}" alt="phoenix"
                                width="80"></div>
            </a>
            <div class="text-center mb-7">
                <h3>Sign In</h3>
                <p class="text-700">Get access to your account</p>
            </div>
            @component('components.flash')
            @endcomponent

            <form class="bg-white" action="{{ route('admin.login') }}" method="post">
                @csrf
                <div class="mb-3 text-start"><label class="form-label" for="email">Email address</label>
                    <div class="form-icon-container"><input name="email" class="form-control form-icon-input" id="email"
                            type="email" placeholder="name@example.com"><span
                            class="fas fa-user text-900 fs--1 form-icon"></span></div>
                </div>
                <div class="mb-3 text-start"><label class="form-label" for="password">Password</label>
                    <div class="form-icon-container"><input name="password" class="form-control form-icon-input"
                            type="password" placeholder="Password"><span
                            class="fas fa-user text-900 fs--1 form-icon"></span></div>
                </div>
                <div class="row flex-between-center mb-7">
                    <div class="col-auto">
                        <div class="form-check mb-0"><input class="form-check-input" id="basic-checkbox" type="checkbox"
                                checked="checked"><label class="form-check-label mb-0" for="basic-checkbox">Remember
                                me</label></div>
                    </div>
                    <div class="col-auto"><a class="fs--1 fw-semi-bold" href="#!">Forgot Password?</a></div>
                </div><button type="submit" class="btn btn-primary w-100 mb-3">Sign In</button>
            </form>

        </div>
    </div>
    {{-- <div class="text-center"><a class="fs--1 fw-bold" href="#!">Create an account</a></div> --}}
</div>
</div>
</div>
@endsection
