@extends('layout.page')

@section('title', 'Product Category')

@section('header-content')

@endsection

@section('page-content')
<x-card.layout >
    <x-slot name="header">
        {{ request()->routeIs('product_category.create') ? 'Create Product Category' : 'Update Product Category' }}
    </x-slot>
    <x-slot name="body">
        <form id="form"
            action="{{ request()->routeIs('product_category.create') ? route('product_category.store'): route('product_category.update', @$productCategory->id) }}"
            class="theme-form mega-form" method="post"
            enctype="multipart/form-data">
            @csrf
            <x-forms.put-method  />
            <x-forms.input label="Name" name="name" :value="@$productCategory->name"/>
        </form>
        <button form="form" class="btn btn-primary btn-pill">Submit</button>
        <x-action.cancel />
    </x-slot>
</x-card.layout>
@endsection

@section('footer-content')

@endsection
