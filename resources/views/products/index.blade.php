@extends('layout.page')

@section('title', 'Product')

@section('header-content')

@endsection

@section('page-content')
<x-card.layout >
    <x-slot name="header">
        <x-card.h-buat url="{{route('product.create')}}" title="Product"/>
    </x-slot>

    <x-slot name="body">
        <table class="table table-striped">
            <thead>
                <th>No</th>
                <th>Name</th>
                <th>Sub Category</th>
                <th>Category</th>
                <th>Action</th>
            </thead>
            <tbody>
                @foreach ($products as $each)
                {{-- @dd($each) --}}
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$each->name}}</td>
                        <td>{{$each->product_subcategory->pluck('name')->implode(', ')}}</td>
                        <td>{{$each->product_subcategory->pluck('product_category.name')->unique()->implode(', ')}}</td>
                        <td>
                            <x-action.edit action="{{route('product.edit', $each->id)}}" />
                            <x-action.delete :ident="$each->id" action="{{route('product.destroy', $each->id)}}" />
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </x-slot>
</x-card.layout>
@endsection

@section('footer-content')

@endsection
