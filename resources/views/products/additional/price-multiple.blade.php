<div class="accordion" id="parentPrice">
    @foreach ($productPrice as $eachPrice)
    <x-forms.accordion parentName="parentPrice" keyword="{{$eachPrice->id}}">
        <x-slot name="header">
            Harga Target : {{ $eachPrice->target_type }}
        </x-slot>
        <x-slot name="body">
            <input type="hidden" name="{{ $eachPrice->target_type }}[price_type]" value="retail">
            <x-forms.input name="{{ $eachPrice->target_type }}[published_price]" required type="number" label="Published Price / Harga Coret" :value="@$eachPrice->published_price"/>
            <x-forms.input name="{{ $eachPrice->target_type }}[buy_price]" required type="number" label="Buy Price" :value="@$eachPrice->buy_price"/>
            <x-forms.input name="{{ $eachPrice->target_type }}[price]" required type="number" label="Price" :value="@$eachPrice->price"/>
            <div class="from-group mb-2">
                <label for="wholesaler">Wholesaler / Grosir</label>
                @if (request()->routeIs('product.edit'))
                <input type="checkbox" name="{{ $eachPrice->target_type }}[wholesaler_checkbox]" id="wholesaler" {{ $eachPrice->price_type == 'retail' ? '' : 'checked' }}>
                @else
                <input type="checkbox" name="{{ $eachPrice->target_type }}[wholesaler_checkbox]" id="wholesaler">
                @endif
            </div>
            <div class="wholeshalerform" id="wholeshalerform">
                @if (request()->routeIs('product.edit') && !empty($eachPrice->product_price_wholesaler))
                {{-- @dd($each->varian_id, $each->min, $each->max, $each->price) --}}
                <div class="form-group wsGroup{{ $eachPrice->target_type }}">
                    @foreach ($eachPrice->product_price_wholesaler as $each)
                    <x-forms.wsgroupitem idWholeSaler="{{$each->id}}" identify="{{$eachPrice->target_type}}" varian="{{$each->varian_id}}" min="{{@$each->min}}" max="{{@$each->max}}" price="{{@$each->price}}" />
                    @endforeach
                </div>
                @else
                <div class="form-group wsGroup">
                    <x-forms.wsgroupitem varian="1" min="1" max="" price="" />
                </div>
                @endif
                <button class="btn btn-success tambahButton mb-2" value="{{$eachPrice->target_type}}" type="button">Tambah</button>
            </div>
        </x-slot>
    </x-forms.accordion>
    @endforeach
</div>
