<input type="hidden" name="price_type" value="retail">
<x-forms.input name="published_price" required type="number" label="Published Price / Harga Coret"
    :value="@$eachPrice->published_price" />
<x-forms.input name="buy_price" required type="number" label="Buy Price" :value="@$eachPrice->buy_price" />
<x-forms.input name="price" required type="number" label="Price" :value="@$eachPrice->price" />
<div class="from-group mb-2">
    <label for="wholesaler">Wholesaler / Grosir</label>
    @if (request()->routeIs('product.edit'))
    <input type="checkbox" name="wholesaler_checkbox" id="wholesaler" {{ $eachPrice->price_type == 'retail' ? '' :
    'checked' }}>
    @else
    <input type="checkbox" name="wholesaler_checkbox" id="wholesaler">
    @endif
</div>
<div class="wholeshalerform" id="wholeshalerform">
    @if (request()->routeIs('product.edit') && !empty($eachPrice->product_price_wholesaler))
    {{-- @dd($each->varian_id, $each->min, $each->max, $each->price) --}}
    <div class="form-group wsGroupSingle">
        @foreach ($eachPrice->product_price_wholesaler as $each)
        <x-forms.wsgroupitem varian="{{$each->varian_id}}" min="{{@$each->min}}" max="{{@$each->max}}"
            price="{{@$each->price}}" />
        @endforeach
    </div>
    @else
    <div class="form-group wsGroupSingle">
        <x-forms.wsgroupitem identify="Single" varian="1" min="1" max="" price="" />
    </div>
    @endif
    <button class="btn btn-success tambahButton mb-2" type="button" value="Single">Tambah</button>
</div>
