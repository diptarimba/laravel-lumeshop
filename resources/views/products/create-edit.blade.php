@extends('layout.page')

@section('title', 'Product')

@section('header-content')
@endsection

@section('page-content')
<x-card.layout>
    <x-slot name="header">
        {{ request()->routeIs('product.create') ? 'Create Product' : 'Update Product' }}
    </x-slot>
    <x-slot name="body">
        <form id="form"
            action="{{ request()->routeIs('product.create') ? route('product.store'): route('product.update', @$product->id) }}"
            class="theme-form mega-form mb-2" method="post" enctype="multipart/form-data">
            @csrf
            <x-forms.put-method />

            @if(request()->routeIs('product.edit'))
            <span class="col-form-label">Product Gallery</span>
            @if (!empty($image))
            <div class="row row-cols-lg-4 row-cols-sm-2 row-cols-md-3 justify-content-center">
                @foreach ($image as $item)
                <div class="col card previewPictDB">
                    <div class="my-auto">
                        <img class="img-thumbnail imagePreview" src="{{ asset('storage/'.$item->image) }}" alt="">
                    </div>
                    <div class="card-footer mx-auto">
                        <button type="button" class="shadow btn btn-danger btn-sm my-2 deletePhoto"
                            id="{{$item->id}}">Delete</button>
                    </div>
                </div>
                @endforeach
            </div>
            @else
            <br>
            <div class="text-center text-danger text-bold">Belum Ada Gambar</div>
            @endif
            <div class="text-center">Tersisa {{ (10 - count($image)) }} slot gambar untuk di upload</div>
            @endif
            <x-forms.file label="Pilih Gambar" name="image[]" multiple id="gallery-photo-add" />
            <div class="gallery row row-cols-4 justify-content-center" id="isi-gallery"></div>
            <x-forms.text label="Product Name" name="name" :value="@$product->name" />
            <x-forms.input label="Stock" type="number" :value="@$product->stock" name="stock" />
            <x-forms.wysiwyg name="description" label="Description" placeholder="Product Description.." :value="@$product->description" manual="1" />
            <x-forms.wysiwyg name="recommendation" label="Recommendation Use" placeholder="Recommendation.." :value="@$product->recommendation" manual="1" />
            @if(request()->routeIs('product.create'))
            <x-forms.select name="product_subcategory_id[]" label="Sub Category" :items="@$productSubcategory"
                :value="@$product->product_subcategory_id" multiple />
            @else
            <x-forms.product-select name="product_subcategory_id[]" label="Sub Category" :items="@$productSubcategory"
                :value="@array_flip($product->product_subcategory->pluck('id')->toArray())" multiple />
            @endif
            @if (request()->routeIs('product.edit'))
                @include('products.additional.price-multiple')
            @else
                @include('products.additional.price-single')
            @endif
        </form>
        <button form="form" class="btn btn-primary btn-pill">Submit</button>
        <x-action.cancel />
    </x-slot>
</x-card.layout>
@endsection

@section('footer-content')
<script src="{{ asset('js/md5.min.js') }}" integrity="sha512-8pbzenDolL1l5OPSsoURCx9TEdMFTaeFipASVrMYKhuYtly+k3tcsQYliOEKTmuB1t7yuzAiVo+yd7SJz+ijFQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $(document).ready(() => {

        var page_types = ['Single', 'ENDUSER', 'RESELLER'];
        page_types.map(function(isi, key){
            if($(`.wsGroup${isi}`).children().length == 5){
                $(`.tambahButton[value="${isi}"]`).prop('disabled', true);
            }

            if($(`input[name="${isi}\\[price_type\\]"]`).val() == 'retail'){
                $(`.wsGroupItem${isi}`).prop('readonly', true)
                $('.tambahButton').prop('disabled', true);
            }

            if(isi !== 'Single')
            {
                $(`input[name="${isi}\\[wholesaler_checkbox\\]"]`).on('change', function(){
                    if($(`input[name="${isi}\\[wholesaler_checkbox\\]"]`).is(':checked')){
                        $(`input[name="${isi}\\[price_type\\]"]`).val('wholesale')
                        $(`.wsGroupItem${isi}`).prop('readonly', false)
                        if($(`.wsGroup${isi}`).children().length !== 5){
                            $(`.tambahButton[value="${isi}"]`).prop('disabled', false);
                        }
                    }else{
                        $(`input[name="${isi}\\[price_type\\]"]`).val('retail')
                        $(`.wsGroupItem${isi}`).prop('readonly', true)
                        $(`.tambahButton[value="${isi}"]`).prop('disabled', true);
                    }
                })
            }else{
                if($('input[name="price_type"]').val() == 'retail'){
                    $('.wsGroupItem').prop('readonly', true)
                    $('.tambahButton').prop('disabled', true);
                }

                $(`input[name="wholesaler_checkbox"]`).on('change', function(){
                    if($(`input[name="wholesaler_checkbox"]`).is(':checked')){
                        $('input[name="price_type"]').val('wholesale')
                        $('.wsGroupItem').prop('readonly', false)
                        if($(`.wsGroup${isi}`).children().length !== 5){
                            $(`.tambahButton[value="${isi}"]`).prop('disabled', false);
                        }
                    }else{
                        $('input[name="price_type"]').val('retail')
                        $('.wsGroupItem').prop('readonly', true)
                        $('.tambahButton').prop('disabled', true);
                    }
                })
            }

        })

        $(document).on('click','.deletePrice', function(){
            var uniqueChar = $(this).attr('data-line-unique');
            var id = $(this).attr('id');
            if(id === null || id === undefined || id === ''){
                $(`.input-group[data-line-unique="${uniqueChar}"]`).remove()
            }else{
                Swal.fire({
                title: 'Apakah anda yakin ingin hapus?',
                text: "Kamu tidak bisa membatalkan setelahnya",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Hapus!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Swal.fire(
                        'Terhapus!',
                        'Data berhasil dihapus.',
                        'success'
                        ).then(() => {
                            $.ajax({
                                url: '/product_price/wholesaler/' + id,
                                type: 'POST',
                                data: {
                                    id : id,
                                    _token: "{{ csrf_token() }}",
                                    _method: "DELETE"
                                },
                                success: function() {
                                    $(`.input-group[data-line-unique="${uniqueChar}"]`).remove()
                                    alert('Berhasil')
                                    // window.location.reload();
                                },
                                error: function(){
                                    alert('Gagal')
                                }
                            });

                        })
                    }
                })
            }

            var page_types = ['Single', 'ENDUSER', 'RESELLER'];
            page_types.map(function(isi, key){
                if($(`.wsGroup${isi}`).children().length == 5){
                    $(`.tambahButton[value="${isi}"]`).prop('disabled', true);
                }else{
                    $(`.tambahButton[value="${isi}"]`).prop('disabled', false);
                }
            })


        })

        $('.summernote').summernote({
            // placeholder: 'Deskripsi produk',
            tabsize: 2,
            height: 100,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ]
        });

        //menghapus foto yang ada di database
        $('.deletePhoto').on('click', function(){
            var id = $(this).attr('id');
            $(`#imagePict option[value='${id}']`).remove();
            $.ajax({
                url: '/product_image/' + id,
                type: 'POST',
                data: {
                    id : id,
                    _token: "{{ csrf_token() }}",
                    _method: "DELETE"
                },
                success: function() {
                    alert('Berhasil')
                    window.location.reload();
                },
                error: function(){
                    alert('Gagal')
                }
            });
        })

        $(document).on('change', '.minvalue', function(){
            var valueChange = $(this).val();
            var allDataMin = $('input.minvalue');
            var allDataMax = $('input.maxvalue');
            var qtyVarianPriceMin = $('.minvalue').length
            if(qtyVarianPriceMin > 1){
                var data = $.map(allDataMin, function(value, index){
                   if(index > 0){
                       if(value.value === valueChange){
                            if(value.value <= allDataMax[index-1].value){
                                alert('Data Harus Lebih dari Kolom Max Range Sebelumnya')
                            }
                       }
                   }
                })
            }

            $.map(allDataMin, function(value, index){
                if(value.value == valueChange){
                    if(value.value >= allDataMax[index].value && allDataMax[index].value !== ''){
                        alert('Kolom Min tidak boleh lebih besar / sama dengan dari Max')
                    }
                }
            })
        })

        $(document).on('change', '.maxvalue', function(){
            var valueChange = $(this).val();
            var allDataMin = $('input.minvalue');
            var allDataMax = $('input.maxvalue');

            $.map(allDataMax, function(value, index){
                if(value.value == valueChange){
                    if(value.value <= allDataMin[index].value && allDataMin[index].value !== ''){
                        alert('Kolom Max tidak boleh lebih kecil / sama dengan dari Min')
                    }
                }
            })
        })

        $(document).on('click', '.tambahButton', function(){
                var type_page = $(this).val();
                var indexRange = [1,2,3,4,5];
                var ek = $(`.varianId${type_page}`).map((_,el) => el.value).get()
                var nomor;
                for(var x = 0; x < indexRange.length; x++){
                    if(!ek.includes(`${indexRange[x]}`)){
                        nomor = indexRange[x];
                        break;
                    }
                }
                var uniqueData = md5(Math.random(1,999)).substr(1, 5)
                var inputGroup = $('<div/>').attr({
                    class: 'input-group mb-2',
                    'data-line-unique': uniqueData
                });
                var spanInput = $('<span/>', {text: 'Min, Max, Price'}).attr({
                    class: 'input-group-text'
                });
                spanInput.appendTo(inputGroup);
                var inputMin = $('<input/>').attr({
                    type: 'number',
                    class: 'form-control wsGroupItem minvalue',
                    placeholder: 'Min',
                    name: type_page == 'single' ? `wholesaler[${nomor}][minimum]` : type_page + `[wholesaler][${nomor}][minimum]`,
                }).prop('required', true);
                inputMin.appendTo(inputGroup);
                var inputMax = $('<input/>').attr({
                    type: 'number',
                    class: 'form-control wsGroupItem maxvalue',
                    placeholder: 'Max',
                    name: type_page == 'single' ? `wholesaler[${nomor}][maximum]` : type_page + `[wholesaler][${nomor}][maximum]`,
                }).prop('required', true);
                inputMax.appendTo(inputGroup);
                var inputPrice = $('<input/>').attr({
                    type: 'number',
                    class: 'wsGroupItem form-control',
                    placeholder: 'Price',
                    name: type_page == 'single' ? `wholesaler[${nomor}][price]` : type_page + `[wholesaler][${nomor}][price]`,
                }).prop('required', true);
                inputPrice.appendTo(inputGroup);
                var inputVarian = $('<input/>').attr({
                    type: 'hidden',
                    class: `wsGroupItem varianId${type_page} form-control`,
                    placeholder: 'Price',
                    name: type_page == 'single' ? `wholesaler[${nomor}][varian_id]` : type_page + `[wholesaler][${nomor}][varian_id]`,
                    value: nomor
                }).prop('required', true);
                inputVarian.appendTo(inputGroup);
                var inputDelete = $('<input/>').attr({
                    class: 'form-control btn btn-danger deletePrice fa-solid fa-xmark',
                    style: 'max-width: 10%',
                    'data-line-unique': uniqueData
                })
                inputDelete.appendTo(inputGroup);
                inputGroup.appendTo(`div.wsGroup${type_page}`);

                if($(`.wsGroup${type_page}`).children().length == 5)
                {
                    $('.tambahButton').prop('disabled', true);
                }else{
                    $('.tambahButton').prop('disabled', false);
                }
        })

        //function untuk menampilkan preview image
        var imagesPreview = function(input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;

                for (var i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();

                    reader.onload = function(event) {
                        var card = $('<div/>').attr({
                            class: 'col mx-1 card'
                        })

                        var divImage = $('<div/>').attr({
                            class: 'my-auto'
                        })

                        $($.parseHTML('<img>')).attr('src', event.target.result).attr('class', 'img-thumbnail imagePreview').appendTo(divImage);

                        divImage.appendTo(card);

                        var divFooter = $('<div/>').attr({
                            class: 'card-footer'
                        })

                        // buttonImage.appendTo(divFooter);
                        divFooter.appendTo(card);
                        card.appendTo(placeToInsertImagePreview);

                        if($('.imagePreview').length > 10){
                            alert('Upload Gambar dibatasi hingga 10 gambar')
                            return;
                        }
                    }

                    reader.readAsDataURL(input.files[i]);
                }
            }

        };

        if($('.previewPictDB').length === 10){
            $('input[name="image\\[\\]"]').prop('readonly', true)
        }

        //Menampilkan Thumbnail sebelum upload
        $('#gallery-photo-add').on('change', function() {
            imagesPreview(this, 'div.gallery');
        });

        //Menghapus Thumbnail apabila terdapat pergantian file upload
        $('#gallery-photo-add').on('click', function(){
            // console.log('Masuk')
            let parent = document.getElementById("isi-gallery")
            while (parent.firstChild) {
                parent.firstChild.remove()
            }
        });


    });
</script>
@endsection
