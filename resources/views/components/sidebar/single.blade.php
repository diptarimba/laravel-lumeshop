<a class="nav-link" href="{{$link}}" role="button" data-bs-toggle="" aria-expanded="false">
    <div class="d-flex align-items-center"><span class="nav-link-icon"><span data-feather="{{$icon}}"></span></span><span
            class="nav-link-text">{{$name}}</span></div>
</a>
