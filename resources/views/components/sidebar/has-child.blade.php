<a class="nav-link dropdown-indicator" href="#parent{{$ident}}" role="button" data-bs-toggle="collapse" aria-expanded="false"
    aria-controls="parent{{$ident}}">
    <div class="d-flex align-items-center">
        <div class="dropdown-indicator-icon d-flex flex-center"><span class="fas fa-caret-right fs-0"></span></div>
        <span
            class="nav-link-icon"><span data-feather="{{$icon}}"></span></span><span class="nav-link-text">{{$name}}</span>
    </div>
</a>
<ul class="nav collapse parent" id="parent{{$ident}}">
    {{ $slot }}
</ul>
