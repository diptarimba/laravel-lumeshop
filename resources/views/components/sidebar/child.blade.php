<li class="nav-item"><a class="nav-link" href="{{ $link }}" data-bs-toggle=""
    aria-expanded="false">
    <div class="d-flex align-items-center"><span class="nav-link-text">{{ $name }}</span></div>
</a></li>
