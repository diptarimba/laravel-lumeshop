<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;

    const ROLE_SUPERADMIN = 'SUPERADMIN';
    const ROLE_ADMIN = 'ADMIN';

    protected $fillable = ['email',  'password', 'image', 'name', 'phone', 'role', 'username'];

    protected $hidden = ['password',  'remember_token'];

    public static function select_role()
    {
        return [ self::ROLE_SUPERADMIN => self::ROLE_SUPERADMIN, self::ROLE_ADMIN => self::ROLE_ADMIN];
    }
}
