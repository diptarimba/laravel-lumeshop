<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'description',
        'recommendation',
        'name',
        'stock'
    ];

    // protected $appends = [
    //     'sub_category'
    // ];

    public function cart()
    {
        return $this->hasMany(Cart::class);
    }

    public function favorite()
    {
        return $this->hasMany(Favorite::class);
    }

    public function product_image()
    {
        return $this->hasMany(ProductImage::class);
    }

    public function product_price()
    {
        return $this->hasMany(ProductPrice::class);
    }

    public function price_dropshipper()
    {
        return $this->hasMany(PriceDropshipper::class);
    }

    public function product_subcategory()
    {
        return $this->belongsToMany(ProductSubcategory::class, 'pivot_product_subcategories')->wherePivot('status', '=', '1');
    }
}
