<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reseller extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'link',
        'link_type_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function link_type()
    {
        return $this->belongsTo(Reseller::class, 'link_type_id');
    }
}
