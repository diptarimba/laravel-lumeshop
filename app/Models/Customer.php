<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function address()
    {
        return $this->hasMany(Address::class);
    }

    public function cart()
    {
        return $this->hasMany(Cart::class);
    }

    public function coin()
    {
        return $this->hasOne(Coin::class);
    }

    public function has_voucher()
    {
        return $this->hasMany(HasVoucher::class);
    }

    public function order()
    {
        return $this->hasMany(Order::class);
    }
}
