<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommandName extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    public function status_list()
    {
        return $this->hasOne(StatusList::class, 'command_name_id');
    }

    public function coin_history()
    {
        return $this->hasMany(CoinHistory::class, 'command_name_id');
    }
}
