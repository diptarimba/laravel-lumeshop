<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusList extends Model
{
    use HasFactory;

    protected $fillable = [
        'command_name_id',
        'status'
    ];

    public function command_name(){
        return $this->belongsTo(CommandName::class, 'command_name_id');
    }
}
