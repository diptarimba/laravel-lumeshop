<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResellerLinkType extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    public function reseller(){
        return $this->hasMany(Reseller::class, 'link_type_id');
    }
}
