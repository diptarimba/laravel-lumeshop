<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderVoucher extends Model
{
    use HasFactory;

    protected $fillable = [
        'store_voucher_id',
        'product_voucher_id'
    ];

    public function store_voucher()
    {
        return $this->belongsTo(Voucher::class, 'store_voucher_id');
    }

    public function product_voucher()
    {
        return $this->belongsTo(Voucher::class, 'product_voucher_id');
    }

    public function order()
    {
        return $this->hasMany(Order::class);
    }
}
