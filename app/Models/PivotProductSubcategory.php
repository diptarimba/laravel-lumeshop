<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class PivotProductSubcategory extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id',
        'product_subcategory_id',
        'status'
    ];

    public $timestamps = true;

    public function product()
    {
        return $this->hasMany(Product::class);
    }

    public function product_subcategory()
    {
        return $this->hasMany(ProductSubcategory::class);
    }
}
