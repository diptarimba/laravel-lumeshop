<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PriceDropshipper extends Model
{
    use HasFactory;

    protected $fillable = [
        'price',
        'buy_price',
        'published_price',
        'price_type',
        'product_id'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function price_wholesaler_dropshipper()
    {
        return $this->hasMany(PriceWholesalerDropshipper::class);
    }
}
