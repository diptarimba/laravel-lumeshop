<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PriceWholesalerDropshipper extends Model
{
    use HasFactory;

    protected $fillable = [
        'min',
        'max',
        'price',
        'price_dropshipper_id',
        'varian_id'
    ];

    public function price_dropshipper()
    {
        return $this->belongsTo(PriceDropshipper::class, 'price_dropshipper_id');
    }
}
