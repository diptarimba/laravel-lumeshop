<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'type'
    ];

    public function has_voucher()
    {
        return $this->hasMany(HasVoucher::class);
    }

    public function order()
    {
        return $this->hasMany(Order::class);
    }

    public function order_voucher()
    {
        return $this->hasMany(OrderVoucher::class);
    }
}
