<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'cart_id',
        'user_id',
        'shipping_id',
        'address_id',
        'payment_method_id',
        'order_voucher_id',
        'sum_quantity',
        'sum_price',
        'coins',
        'resi'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function voucher()
    {
        return $this->belongsTo(Voucher::class);
    }

    public function payment_method()
    {
        return $this->belongsTo(PaymentMethod::class);
    }

    public function order_voucher()
    {
        return $this->belongsTo(OrderVoucher::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }

    public function shipping()
    {
        return $this->belongsTo(Shipping::class);
    }
}
