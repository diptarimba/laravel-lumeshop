<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CoinHistory extends Model
{
    use HasFactory;

    protected $fillable = [
        'coin_id',
        'command_name_id',
        'amount'
    ];

    public function coin(){
        return $this->belongsTo(Coin::class, 'coin_id');
    }

    public function command_name()
    {
        return $this->belongsTo(CommandName::class, 'command_name_id');
    }
}
