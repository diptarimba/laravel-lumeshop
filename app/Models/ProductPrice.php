<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductPrice extends Model
{
    use HasFactory;

    const TARGET_TYPE_RESELLER = 'RESELLER';
    const TARGET_TYPE_ENDUSER = 'ENDUSER';

    protected $fillable = [
        'price',
        'buy_price',
        'published_price',
        'price_type',
        'product_id',
        'target_type'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function product_price_wholesaler()
    {
        return $this->hasMany(ProductPriceWholesaler::class);
    }

    public static function target_type()
    {
        return [self::TARGET_TYPE_RESELLER, self::TARGET_TYPE_ENDUSER];
    }
}
