<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coin extends Model
{
    use HasFactory;

    protected $fillable = [
        'value',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function coin_history()
    {
        return $this->hasMany(CoinHistory::class, 'coin_id');
    }
}
