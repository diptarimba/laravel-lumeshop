<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductPriceWholesaler extends Model
{
    use HasFactory;

    protected $fillable = [
        'min',
        'max',
        'price',
        'product_price_id',
        'varian_id'
    ];

    public function product_price()
    {
        return $this->belongsTo(ProductPrice::class, 'product_price_id');
    }
}
