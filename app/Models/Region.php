<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public function address()
    {
        return $this->hasMany(Address::class);
    }

    public function city()
    {
        return $this->hasMany(City::class);
    }
}
