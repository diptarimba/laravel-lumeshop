<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\HasVoucher;
use Illuminate\Http\Request;

class HasVoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hasVouchers = HasVoucher::all();

        return view('hasVouchers.index', compact('hasVouchers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('hasVouchers.create-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            "user_id" => "sometimes|exists:users,id",
            "voucher_id" => "required|exists:vouchers,id"
        ]);

        HasVoucher::create($request->all());

        // return redirect(route())->with('status', 'item stored successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HasVoucher $hasVoucher
     * @return \Illuminate\Http\Response
     */
    public function show(HasVoucher $hasVoucher)
    {
        return view('hasVouchers.show', compact('hasVoucher'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HasVoucher $hasVoucher
     * @return \Illuminate\Http\Response
     */
    public function edit(HasVoucher $hasVoucher)
    {
        return view('hasVouchers.create-edit', compact('hasVoucher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\HasVoucher $hasVoucher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HasVoucher $hasVoucher)
    {
        $validated = $this->validate($request, [
            "user_id" => "sometimes|exists:users,id",
            "voucher_id" => "required|exists:vouchers,id"
        ]);

        $hasVoucher->update($validated);

        // return redirect(route())->with('success', 'item updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HasVoucher $hasVoucher
     * @return \Illuminate\Http\Response
     */
    public function destroy(HasVoucher $hasVoucher)
    {
        $hasVoucher->delete();

        // return redirect(route())->with('success', 'item deleted successfully');
    }
}
