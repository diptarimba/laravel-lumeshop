<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SliderPromo;
use Illuminate\Http\Request;

class SliderPromoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliderPromos = SliderPromo::all();

        return view('sliderPromos.index', compact('sliderPromos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sliderPromos.create-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            "name" => "required",
            "image" => "required|max:1024|mimes:png,jpg,jpeg",
            "exp_date" => "required"
        ]);

        SliderPromo::create(array_merge($request->toArray(), [
            'image' => $request->hasFile('image') ? $request->file('image')->storePublicly('slider', 'public') : null
        ]));

        return redirect(route('slider_promo.index'))->with('status', 'item stored successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SliderPromo $sliderPromo
     * @return \Illuminate\Http\Response
     */
    public function show(SliderPromo $sliderPromo)
    {
        return view('sliderPromos.show', compact('sliderPromo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SliderPromo $sliderPromo
     * @return \Illuminate\Http\Response
     */
    public function edit(SliderPromo $sliderPromo)
    {
        return view('sliderPromos.create-edit', compact('sliderPromo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\SliderPromo $sliderPromo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SliderPromo $sliderPromo)
    {
        $validated = $this->validate($request, [
            "name" => "required",
            "image" => "sometimes|max:1024|mimes:png,jpg,jpeg",
            "exp_date" => "required"
        ]);

        $sliderPromo->update(array_merge($request->toArray(), [
            'image' => $request->hasFile('image') ? $request->file('image')->storePublicly('slider', 'public') : $sliderPromo->image
        ]));

        return redirect(route('slider_promo.index'))->with('success', 'item updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SliderPromo $sliderPromo
     * @return \Illuminate\Http\Response
     */
    public function destroy(SliderPromo $sliderPromo)
    {
        $sliderPromo->delete();

        return redirect(route('slider_promo.index'))->with('success', 'item deleted successfully');
    }
}
