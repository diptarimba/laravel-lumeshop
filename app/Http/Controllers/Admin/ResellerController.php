<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Coin;
use App\Models\Reseller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ResellerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resellers = Reseller::with('user')->get();

        return view('resellers.index', compact('resellers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('resellers.create-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            "name" => "required",
            "email" => "required|unique:users,email",
            "username" => "required|unique:users,username",
            "birthdate" => "required",
            "phone_number" => "required|unique:users,phone_number",
            "password" => "required",
            "shopee_link" => "sometimes",
            "tokped_link" => "sometimes",
            "bukalapak_link" => "sometimes"
        ]);

        $user = User::create(array_merge($request->all(), ["password" => Hash::make($request->password)]));
        $reseller = Reseller::create(array_merge($request->only(["shopee_link", "tokped_link", "bukalapak_link"]), ["user_id" => $user->id]));
        $coin = Coin::create(["user_id" => $user->id, "value" => 0]);

        return redirect(route('reseller.index'))->with('status', 'item stored successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reseller $reseller
     * @return \Illuminate\Http\Response
     */
    public function show(Reseller $reseller)
    {
        return view('resellers.show', compact('reseller'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Reseller $reseller
     * @return \Illuminate\Http\Response
     */
    public function edit(Reseller $reseller)
    {
        return view('resellers.create-edit', compact('reseller'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Reseller $reseller
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reseller $reseller)
    {
        $validated = $this->validate($request, [
            "name" => "required",
            "email" => "required|unique:users,email",
            "username" => "required|string|unique:users,username",
            "birthdate" => "required",
            "phone_number" => "required|unique:users,phone_number",
            "shopee_link" => "sometimes",
            "tokped_link" => "sometimes",
            "bukalapak_link" => "sometimes",
            "coins_value" => "sometimes|integer"
        ]);

        $reseller->user->first()->update(array_merge($request->all(),
            ['password' => $request->password ? Hash::make($request->password) : $reseller->user->first()->password]));
        $reseller->update($request->all());
        $reseller->coin->first()->update(['value' => $request->coins_value ?? $reseller->coin->value]);

        return redirect(route('reseller.index'))->with('success', 'item updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Reseller $reseller
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reseller $reseller)
    {
        $reseller->delete();

        return redirect(route('reseller.index'))->with('success', 'item deleted successfully');
    }
}
