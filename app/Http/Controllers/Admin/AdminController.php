<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = Admin::all();

        return view('admins.index', compact('admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $select_role = Admin::select_role();
        return view('admins.create-edit', compact('select_role'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            "email" => "required",
            "password" => "required|min:6",
            "image" => "required|max:1024|mimes:jpg,png,jpeg",
            "name" => "required|min:5",
            "phone" => "required",
            "role" => "required|in:SUPERADMIN,ADMIN",
            "username" => "required|min:5"
        ]);

        Admin::create(array_merge($request->all(),[
            'password' => bcrypt($request->password),
            'image' => $request->hasFile('image') ? $request->file('image')->storePublicly('admin/avatar') : null
        ]));

        return redirect(route('admin.index'))->with('status', 'item stored successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        return view('admins.show', compact('admin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        $select_role = Admin::select_role();
        return view('admins.create-edit', compact('admin', 'select_role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Admin $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        $validated = $this->validate($request, [
            "email" => "required",
            "password" => "sometimes",
            "image" => "sometimes|max:1024|mimes:jpg,png,jpeg",
            "name" => "required|min:5",
            "phone" => "required",
            "role" => "required|in:SUPERADMIN,ADMIN",
            "username" => "required|min:5"
        ]);
        // dd(array_merge($request->all(),[
        //     'password' => bcrypt($request->password),
        //     'image' => $request->hasFile('image') ? $request->file('image')->storePublicly('admin/avatar') : null
        // ]));
        $admin->update(array_merge($request->all(),[
            'password' => $request->password !== '' ? bcrypt($request->password) : $admin->password,
            'image' => $request->hasFile('image') ? $request->file('image')->storePublicly('admin/avatar') : null
        ]));

        return redirect(route('admin.index'))->with('success', 'item updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        $admin->delete();

        return redirect(route('admin.index'))->with('success', 'item deleted successfully');
    }
}
