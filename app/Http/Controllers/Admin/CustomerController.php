<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Coin;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::with('user')->get();
        return view('customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            "name" => "required",
            "email" => "required|unique:users,email",
            "username" => "required|unique:users,username",
            "birthdate" => "required",
            "phone_number" => "required|unique:users,phone_number",
            "password" => "required",
        ]);

        $user = User::create(array_merge($request->all(), ["password" => Hash::make($request->password)]));
        $customer = Customer::create(["user_id" => $user->id]);
        $coin = Coin::create(["user_id" => $user->id, "value" => 0]);



        return redirect(route('customer.index'))->with('status', 'item stored successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        return view('customers.show', compact('customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        return view('customers.create-edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $validated = $this->validate($request, [
            "name" => "required",
            "email" => "required|unique:users,email",
            "username" => "required|string|unique:users,username",
            "birthdate" => "required",
            "phone_number" => "required|unique:users,phone_number",
            "coins_value" => "sometimes|integer"
        ]);

        $customer->user->first()->update(array_merge($request->all(),
            ['password' => $request->password ? Hash::make($request->password) : $customer->user->first()->password]));
        // $customer->update($request->all());
        $customer->coin->first()->update(['value' => $request->coins_value ?? $customer->coin->value]);

        $customer->update($validated);

        return redirect(route('customer.index'))->with('success', 'item updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        $customer->delete();

        return redirect(route('customer.index'))->with('success', 'item deleted successfully');
    }
}
