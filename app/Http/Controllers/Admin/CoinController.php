<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Coin;
use App\Models\CoinHistory;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class CoinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coins = Coin::all();

        return view('coins.index', compact('coins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('coins.create-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            "value" => "required",
            "user_id" => "required|exists:users,id",
        ]);

        Coin::create($request->all());

        return redirect(route('coin.index'))->with('status', 'item stored successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Coin $coin
     * @return \Illuminate\Http\Response
     */
    public function show(Coin $coin)
    {
        return view('coins.show', compact('coin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Coin $coin
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Coin $coin)
    {
        $choice = [
            1 => 'Tambah',
            2 => 'Kurangi'
        ];

        if($request->ajax()){
            return DataTables(CoinHistory::with(['coin.user:id,name', 'command_name.status_list'])->whereCoinId($coin->id)->get())->toJson();
        }

        return view('coins.create-edit', compact('coin', 'choice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Coin $coin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coin $coin)
    {
        $validated = $this->validate($request, [
            "amount" => "required|integer|min:1",
            "operation" => "required",
            "user_id" => "sometimes|exists:users,id",
        ]);

        if($request->operation == 1){
            $coin->increment('value', $request->amount);
        }else{
            $coin->decrement('value', $request->amount);
        }

        CoinHistory::create([
            'coin_id' => $coin->id,
            'command_name_id' => $request->operation,
            'amount' => $request->amount
        ]);

        return back()->with('success', 'Coin updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Coin $coin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coin $coin)
    {
        $coin->delete();

        return redirect(route('coin.index'))->with('success', 'item deleted successfully');
    }

    public function coinhistory(Coin $coin)
    {
        return DataTables(CoinHistory::with(['coin.user:id,name', 'command_name.status_list'])->whereCoinId($coin->id)->get())->toJson();
    }
}
