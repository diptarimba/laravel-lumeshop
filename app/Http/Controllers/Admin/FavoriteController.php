<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Favorite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FavoriteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $favorites = Favorite::all();
        $favorites = Favorite::select(DB::raw('count(*) as count'), 'product_id')->GroupBy('product_id')->get();

        return view('favorites.index', compact('favorites'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('favorites.create-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            "user_id" => "required|exists:users,id",
            "product_id" => "required|exists:products,id"
        ]);

        Favorite::create($request->all());

        return redirect(route('favorite.index'))->with('status', 'item stored successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Favorite $favorite
     * @return \Illuminate\Http\Response
     */
    public function show(Favorite $favorite)
    {
        return view('favorites.show', compact('favorite'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Favorite $favorite
     * @return \Illuminate\Http\Response
     */
    public function edit(Favorite $favorite)
    {
        return view('favorites.create-edit', compact('favorite'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Favorite $favorite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Favorite $favorite)
    {
        $validated = $this->validate($request, [
            "user_id" => "sometimes|exists:users,id",
            "product_id" => "required|exists:products,id"
        ]);

        $favorite->update($validated);

        return redirect(route('favorite.index'))->with('success', 'item updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Favorite $favorite
     * @return \Illuminate\Http\Response
     */
    public function destroy(Favorite $favorite)
    {
        $favorite->delete();

        return redirect(route('favorite.index'))->with('success', 'item deleted successfully');
    }
}
