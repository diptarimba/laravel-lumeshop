<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ProductCategory;
use App\Models\ProductSubcategory;
use Illuminate\Http\Request;

class ProductSubcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productSubcategories = ProductSubcategory::with('product_category')->get();

        return view('productSubcategories.index', compact('productSubcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $productCategory = ProductCategory::pluck('name', 'id');
        return view('productSubcategories.create-edit', compact('productCategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            "name" => "required",
            "product_category_id" => "required|exists:product_categories,id"
        ]);

        ProductSubcategory::create($request->all());

        return redirect(route('product_subcategory.index'))->with('status', 'item stored successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductSubcategory $productSubcategory
     * @return \Illuminate\Http\Response
     */
    public function show(ProductSubcategory $productSubcategory)
    {
        return view('productSubcategories.show', compact('productSubcategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductSubcategory $productSubcategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductSubcategory $productSubcategory)
    {
        $productCategory = ProductCategory::pluck('name', 'id');
        return view('productSubcategories.create-edit', compact('productSubcategory', 'productCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\ProductSubcategory $productSubcategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductSubcategory $productSubcategory)
    {
        $validated = $this->validate($request, [
            "name" => "required",
            "product_category_id" => "required|exists:product_categories,id"
        ]);

        $productSubcategory->update($validated);

        return redirect(route('product_subcategory.index'))->with('success', 'item updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductSubcategory $productSubcategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductSubcategory $productSubcategory)
    {
        $validate = $productSubcategory->product()->count();

        if($validate > 0){
            return back()->with('error', 'Failed to delete, this subcategory has children data!');
        }

        $productSubcategory->delete();

        return redirect(route('product_subcategory.index'))->with('success', 'item deleted successfully');
    }
}
