<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\OrderVoucher;
use Illuminate\Http\Request;

class OrderVoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orderVouchers = OrderVoucher::all();

        return view('orderVouchers.index', compact('orderVouchers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('orderVouchers.create-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            "store_voucher_id" => "sometimes",
            "product_voucher_id" => "sometimes"
        ]);

        OrderVoucher::create($request->all());

        // return redirect(route())->with('status', 'item stored successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrderVoucher $orderVoucher
     * @return \Illuminate\Http\Response
     */
    public function show(OrderVoucher $orderVoucher)
    {
        return view('orderVouchers.show', compact('orderVoucher'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OrderVoucher $orderVoucher
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderVoucher $orderVoucher)
    {
        return view('orderVouchers.create-edit', compact('orderVoucher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\OrderVoucher $orderVoucher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderVoucher $orderVoucher)
    {
        $validated = $this->validate($request, [
            "store_voucher_id" => "sometimes",
            "product_voucher_id" => "sometimes"
        ]);

        $orderVoucher->update($validated);

        // return redirect(route())->with('success', 'item updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrderVoucher $orderVoucher
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderVoucher $orderVoucher)
    {
        $orderVoucher->delete();

        // return redirect(route())->with('success', 'item deleted successfully');
    }
}
