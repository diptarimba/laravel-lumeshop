<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ProductPrice;
use App\Models\ProductPriceWholesaler;
use Illuminate\Http\Request;

class ProductPriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productPrices = ProductPrice::all();

        return view('productPrices.index', compact('productPrices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('productPrices.create-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            "price" => "required",
            "buy_price" => "required",
            "published_price" => "required",
            "price_type" => "required",
            "product_id" => "required|exists:products,id"
        ]);

        ProductPrice::create($request->all());

        return redirect(route('product_price.index'))->with('status', 'item stored successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductPrice $productPrice
     * @return \Illuminate\Http\Response
     */
    public function show(ProductPrice $productPrice)
    {
        return view('productPrices.show', compact('productPrice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductPrice $productPrice
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductPrice $productPrice)
    {
        return view('productPrices.create-edit', compact('productPrice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\ProductPrice $productPrice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductPrice $productPrice)
    {
        $validated = $this->validate($request, [
            "price" => "required",
            "buy_price" => "required",
            "published_price" => "required",
            "price_type" => "required",
            "product_id" => "required|exists:products,id"
        ]);

        $productPrice->update($validated);

        return redirect(route('product_price.index'))->with('success', 'item updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductPrice $productPrice
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductPrice $productPrice)
    {
        $productPrice->delete();

        return redirect(route('product_price.index'))->with('success', 'item deleted successfully');
    }

    public function destroyWholesaler(ProductPriceWholesaler $wholesaler)
    {

        $wholesaler->delete();

        return back()->with('success', 'item deleted successfully');;
    }
}
