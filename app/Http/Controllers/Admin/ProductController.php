<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PivotProductSubcategory;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductPrice;
use App\Models\ProductPriceWholesaler;
use App\Models\ProductSubcategory;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Http\Request;
use stdClass;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with('product_subcategory.product_category')->get();
        // dd($products->product_subcategory->pluck('product_category.name'));
        return view('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $productSubcategory = ProductSubcategory::pluck('name', 'id');
        return view('products.create-edit', compact('productSubcategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validated = $this->validate($request, [
            "description" => "required",
            "recommendation" => "required",
            "name" => "required",
            "stock" => "required",
            "price_type" => "required",
            "buy_price" => "required",
            "published_price" => "required",
            "product_subcategory_id" => "required",
            "image" => "required|array|max:10",
            "image.*" => "required|max:1024|mimes:png,jpg,jpeg",
        ], [
            "image.*.required" => 'Minimal 1 foto dibutuhkan',
            'image.*.mimes' => 'Tipe foto hanya diperbolehkan berekstensi JPG, PNG, JPG',
            'image.*.max' => 'Ukuran foto hanya diperbolehkan berukuran maks. 1MB'
        ]);

        if($request->price_type == 'retail'){
            $this->validate($request, [
                "price" => "required|integer",
            ]);
        }else{
            $this->validate($request, [
                "Single.wholesaler.*.minimum" => "required|integer|min:1",
                "Single.wholesaler.*.maximum" => "required|integer|min:1",
                "Single.wholesaler.*.price" => "required|integer|min:1",
            ]);
        }
        // dd($request->all(), $request->Single['wholesaler']);

        $product = Product::create($request->all());

        $images = $request->file('image') ?? [];
        $noImage = 0;
        $dataImage = [];

        foreach($images as $image) {
            $dataImage[$noImage]['image'] = $image->storePublicly('product');
            $dataImage[$noImage]['product_id'] = $product->id;
            $noImage++;
        }

        if($request->price_type == 'retail'){
            $dataInputPrice = array_map(function($each) use($request, $product){
                $dataWantCreate = array_merge($request->all(), [
                    'product_id' => $product->id,
                    'target_type' => $each
                ]);
                return ProductPrice::create($dataWantCreate);
            }, ProductPrice::target_type());
        }else{
            $dataInputPrice = array_map(function($each) use($request, $product){
                $dataWantCreate = array_merge($request->all(), [
                    'product_id' => $product->id,
                    'target_type' => $each
                ]);
                return ProductPrice::create($dataWantCreate);
            }, ProductPrice::target_type());

            // Loop for RETAILER and ENDUSER
            $inputWholeSaler = array_map(function($each) use ($request){
                // Loop for each Price
                $eachWholeSaler = array_map(function($byPrice) use ($each){
                    // Create Setiap Data
                    $dataWantCreate = [
                        'min' => $byPrice['minimum'],
                        'max' => $byPrice['maximum'],
                        'price' => $byPrice['price'],
                        'varian_id' => $byPrice['varian_id'],
                        'product_price_id' => $each->id
                    ];
                    return ProductPriceWholesaler::create($dataWantCreate);
                }, $request->Single['wholesaler']);
                return $eachWholeSaler;
            }, $dataInputPrice);
        }

        $inputImage = ProductImage::insert($dataImage);

        $data = array_map(function ($each) use ($product){
            return [
                'product_id' => $product->id,
                'product_subcategory_id' => $each,
                'status' => 1
            ];
        }, $request->input('product_subcategory_id', []));

        $productSubcategory = PivotProductSubcategory::insert($data);

        return redirect(route('product.index'))->with('status', 'item stored successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $productSubcategory = ProductSubcategory::pluck('name', 'id');
        $image = ProductImage::whereProductId($product->id)->get();
        $productPrice = ProductPrice::whereProductId($product->id)->get();
        // dd($productPrice);
        return view('products.create-edit', compact('product', 'productPrice', 'productSubcategory', 'image'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $validated = $this->validate($request, [
            "recommendation" => "required",
            "description" => "required",
            "stock" => "required",

            "product_subcategory_id" => "required",
            "image" => "sometimes|array|max:10",
            "image.*" => "sometimes|max:1024|mimes:png,jpg,jpeg",
        ], [
            // "image.*.required" => 'Minimal 1 foto dibutuhkan',
            'image.*.mimes' => 'Tipe foto hanya diperbolehkan berekstensi JPG, PNG, JPG',
            'image.*.max' => 'Ukuran foto hanya diperbolehkan berukuran maks. 1MB'
        ]);

        array_map(function($each) use ($request){
            $this->validate($request, [
                "$each.price_type" => "required",
                "$each.buy_price" => "required",
                "$each.published_price" => "required",
                "$each.price" => "required|integer",
                "$each.wholesaler.*.minimum" => "required|integer|min:1",
                "$each.wholesaler.*.maximum" => "required|integer|min:1",
                "$each.wholesaler.*.price" => "required|integer|min:1",
            ]);
        }, ProductPrice::target_type());

        $product->update($request->all());

        $data = array_map(function ($each) use ($product){
            return [
                'product_id' => $product->id,
                'product_subcategory_id' => $each
            ];
        }, $request->input('product_subcategory_id', []));

        $updateActive = array_map(function($check){
            PivotProductSubcategory::updateOrCreate($check, ['status' => 1]);
        }, $data);

        $images = $request->file('image') ?? [];
        $noImage = 0;
        $dataImage = [];

        foreach($images as $image) {
            $dataImage[$noImage]['image'] = $image->storePublicly('product');
            $dataImage[$noImage]['product_id'] = $product->id;
            $noImage++;
        }

        $LoopByTarget = array_map(function($eachTarget) use ($request, $product){
            $dataEach = json_decode(json_encode($request->{$eachTarget})); //Ini ngakalin biar bisa pakai panah
            $nextData = $request->{$eachTarget};
            $resRetail = ProductPrice::updateOrCreate(
                ['product_id' => $product->id, 'target_type' => $eachTarget],
                [
                    'price_type' => $dataEach->price_type,
                    'published_price' => $dataEach->published_price,
                    'buy_price' => $dataEach->buy_price,
                    'price' => $dataEach->price
                ]
            );

            $dataWholesaler = array_map(function($eachGrosir) use ($resRetail){
                $eachGrosir = json_decode(json_encode($eachGrosir)); //Ini ngakalin biar bisa pakai panah
                ProductPriceWholesaler::updateOrCreate(
                    ['varian_id' => $eachGrosir->varian_id, 'product_price_id' => $resRetail->id],
                    [
                        'min' => $eachGrosir->minimum,
                        'max' => $eachGrosir->maximum,
                        'price' => $eachGrosir->price
                    ]
                );
            }, $nextData['wholesaler']);

        }, ProductPrice::target_type());

        $inputImage = ProductImage::insert($dataImage);

        $updateDeactive = PivotProductSubcategory::where('product_id', '=', $product->id )->whereNotIn('product_subcategory_id', $request->product_subcategory_id)->update(['status' => 0]);
        return redirect(route('product.index'))->with('success', 'item updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $pivotDelete = PivotProductSubcategory::whereProductId($product->id)->delete();

        $product->delete();

        return redirect(route('product.index'))->with('success', 'item deleted successfully');
    }
}
