<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Reseller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $totalProduct = Product::count();
        $totalReseller = Reseller::count();
        $totalCustomer = Reseller::count();
        return view('dashboard.index', compact('totalProduct', 'totalReseller', 'totalCustomer'));
    }
}
