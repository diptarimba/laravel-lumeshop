<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class AdminLoginController extends Controller
{
    public function index()
    {
        if(Auth::guard('admin')->check()){
            return redirect(route('dashboard'));
        }
        return view('auth.signin');
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|exists:admins,email',
            'password' => 'required'
        ]);

        $auth = Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->filled('remember'));

        if($auth){
            return redirect()
				->intended(route('dashboard'))
				->with('status','Sukses Login Sebagai Admin!');
        }else{
            return back()->withErrors('username / password anda salah!');
        }
    }

    public function logout()
    {
        if (Auth::guard('admin')->check()) {
			Auth::guard('admin')->logout();
			session()->flush();
			return redirect(route('admin.login.index'));
		}
    }
}
